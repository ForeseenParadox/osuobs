Welcome to osu!obs!

The purpose of this project is to allow users to display information in the OBS streaming client that is not currently supported by the vanilla streaming client.
For example, the client currently only supports showing the score of players. In an accuracy based tournament, you obviously only want to display the accuracy of each of the players.
In theory, this OBS plugin will allow you to layer additional information on top of the tourney client such as accuracy by accessing current multiplayer session information.

I realize that this may be a feature implemented with osu!lazer, but there are still tourneys which would like to use this feature now such as Accurney.